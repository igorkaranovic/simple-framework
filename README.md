Simple Framework

#### Requrements
- PHP >= 7.2.0

### Installation
- Copy all files source to web server root.
- Only `public` folder should be visible to public (ServerName: framework.my -> DocumentRoot: /var/www/html/public/)
- Create MySql database
- Import simple-framework.sql in database
- Update `config.ini` file in `root/app/Config/` directory

### Frontend
- Angular framework
- One page based application
- All files are located at `root/public`
- Repo: https://bitbucket.org/igorkaranovic/simple-framework/
