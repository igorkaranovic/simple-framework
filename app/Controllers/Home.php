<?php

namespace App\Controllers;

use Core\View;

class Home {

    public function welcome() {
        View::render('home.php');
    }
}