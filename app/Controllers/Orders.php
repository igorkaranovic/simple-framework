<?php

namespace App\Controllers;

use App\Models\Customer;
use App\Models\Order;
use Core\Controller;
use DateTime;

class Orders extends Controller {

    public function index(string $from = null, string $until = null)  {
        $from = $from === null ? new DateTime('-1 month') : $this->getDateFromString($from);
        $until = $until === null ? new DateTime('now') : $this->getDateFromString($until);

        // get
        $data = [
            'orders' => Order::getCount($from, $until),
            'revenue' => Order::getRevenue($from, $until, 2),
            'customers' => Customer::getCount($from, $until),
            'graph' => [
                'orders' => Order::getCountByDate($from, $until),
                'customers' => Customer::getCountByDate($from, $until)
            ]
        ];

        $this->success($data);
    }

    /**
     * Parse date from string and convert to DateTime object
     *
     * @param string $value - date
     * @param string|null $format - $value date format, default: d-m-Y
     * @return DateTime
     */
    private function getDateFromString(string $value, string $format = null) : DateTime {
        $format = $format ?? 'd-m-Y';
        return DateTime::createFromFormat($format, $value);
    }

}