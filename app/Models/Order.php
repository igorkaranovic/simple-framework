<?php

namespace App\Models;

use Core\Database;
use Core\Model;
use PDO;

class Order extends Model {

    public $table = 'order';

    public function __construct()
    {
        parent::__construct();
        $this->setTable($this->table);
    }

    public static function getCountByDate(\DateTime $from, \DateTime $until) {
        $bind =  [
            'from' => $from->format('Y-m-d'),
            'until' => $until->format('Y-m-d'),
        ];

        $db = Database::getInstance();
        $sql = "SELECT `date`, COUNT(`date`) AS count
                FROM `order` as o
                WHERE date >= :from and date <= :until
                GROUP BY `date` 
                ORDER BY `date` ASC";

        $pdo = $db->executeSQL($sql, $bind);
        return $pdo->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getRevenue(\DateTime $from, \DateTime $until, int $decimals = 2) {
        $bind =  [
            'from' => $from->format('Y-m-d'),
            'until' => $until->format('Y-m-d'),
        ];

        $order = new self();
        $sql = "SELECT SUM(oi.quantity * oi.price) AS total
                FROM `order` as o
                JOIN order_items as oi ON o.id = oi.order_id
                WHERE date >= :from and date <= :until";

        $result = $order->getColumn($sql, $bind);
        if (!$result)
            return 0;

        return round($result, $decimals);
    }

    public static function getCount(\DateTime $from, \DateTime $until, string $country = null) {
        $bind =  [
            'from' => $from->format('Y-m-d'),
            'until' => $until->format('Y-m-d'),
        ];

        $where = '';
        if ($country !== null) {
            $where = " and country = :country";
            $bind['country'] = $country;
        }

        $order = new self();
        $sql = "SELECT count(*) FROM `order` WHERE date >= :from and date <= :until {$where}";
        return $order->getColumn($sql, $bind);
    }

}