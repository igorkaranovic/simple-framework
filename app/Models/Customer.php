<?php

namespace App\Models;

use Core\Database;
use Core\Model;
use PDO;

class Customer extends Model {

    public $table = 'customer';

    public function __construct()
    {
        parent::__construct();
        $this->setTable($this->table);
    }

    public static function getCount(\DateTime $from, \DateTime $until, string $country = null) {
        $bind =  [
            'from' => $from->format('Y-m-d'),
            'until' => $until->format('Y-m-d'),
        ];

        $where = '';
        if ($country !== null) {
            $where = " and country = :country";
            $bind['country'] = $country;
        }

        $customer = new self();
        $sql = "SELECT count(*) 
                FROM `{$customer->getDatabaseName()}`.`customer` 
                WHERE created_at >= :from AND 
                created_at <= :until {$where}";
        return $customer->getColumn($sql, $bind);
    }

    public static function getCountByDate(\DateTime $from, \DateTime $until)
    {
        $bind = [
            'from' => $from->format('Y-m-d'),
            'until' => $until->format('Y-m-d'),
        ];

        $db = Database::getInstance();
        $sql = "SELECT 
                    DATE(c.created_at) as `date`, 
                    COUNT(DATE(c.created_at)) AS count
                FROM `{$db->getName()}`.`customer` as c
                WHERE c.created_at >= :from AND c.created_at <= :until
                GROUP BY `date` 
                ORDER BY `date` ASC";

        $pdo = $db->executeSQL($sql, $bind);
        return $pdo->fetchAll(PDO::FETCH_ASSOC);
    }

}