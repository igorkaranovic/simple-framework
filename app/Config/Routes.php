<?php
/**
 * Routes
 *
 * Here are stored routes. To add new route:
 * $router->add('/posts/latest/', 'ControllerName@method');
 *
 * You can use parameters in routes. Parameter MUST have prefix : (eg. :id)
 * Sample: $router->add('/posts/:id/', 'Posts@get');
 *
 * To validate route parameters, call where method and add regex patter for specific parameter
 * Simple: Sample: $router->add('/posts/:id/', 'Posts@get')->where('id', '[0-9]+');
 *
 * API
 * if route have API flag it will return content as application/json
 *  $router->add('/posts/:id/', 'Posts@get')->where('id', '[0-9]+')->isApi(true);
 */

$router->add('/', 'Home@welcome');
$router->add('/home', 'Home@welcome');

$router->add('orders', 'Orders@index')->isApi(true);
$router->add('orders/:from/:until', 'Orders@index')
    ->where('from', '[\d]{2}-[\d]{2}-[\d]{4}')
    ->where('until', '[\d]{2}-[\d]{2}-[\d]{4}')
    ->isApi(true);


