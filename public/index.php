<?php

use Core\Autoloader;
use Core\Router;

/**
 * Paths
 */
define('ROOT_PATH', dirname(__FILE__,2) . DIRECTORY_SEPARATOR);
define('APP_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR);

/**
 * Config
 */
$config = parse_ini_file(APP_PATH . 'Config/config.ini');
define('CONFIG', $config);

/**
 * Autoloader
 */
require_once ROOT_PATH . 'core/autoloader.php';
Autoloader::register();

/**
 * Router
 */
$router = new Router();
require_once (APP_PATH . '/Config/Routes.php');

$requiredRoute = $_SERVER['REQUEST_URI'] ?? '';
$route = $router->match($requiredRoute);
if (!$route) {
    Router::returnNotFound();
}

try {
    $route->dispatch();
} catch (Exception $e) {
    var_dump($e->getMessage());
}
?>