<?php

namespace Core;

class Router
{

    /**
     * All routes are stored here
     * @var array
     */
    private $routes = [];

    /**
     * Add route to router
     *
     * @param string $route
     * @param string $controller
     * @return Router
     */
    public function add(string $route, string $controller) : Router
    {
        $item = new Route($route);
        $item->setController($controller);

        array_push($this->routes, $item);
        return $this;
    }

    /**
     * Check does requested route match any route in routes
     *
     * @param string $url
     * @return bool|mixed
     */
    public function match(string $url)
    {
        $current = new Route($url);

        foreach ($this->routes as $route) {
            if ($current->segmentsNumber !== $route->segmentsNumber)
                continue;

            // for simple routes with only one segment
            if ($current->segmentsNumber === 1 && $current->route === $route->segments[0]) {
                return $route;
            }

            // for route with multiple segments
            $routesMatch = Route::compare($current, $route);
            if ($routesMatch) {
               return $route;
            }
        }

        return false;
    }

    /**
     * Set is route API
     *
     * @param bool $value
     * @return $this
     */
    public function isApi(bool $value = true)
    {
        $route = end($this->routes);
        $route->setIsApi($value);

        return $this;
    }

    /**
     * Set regex pattern to param in last added route
     *
     * @param string $variable
     * @param string $pattern
     * @return $this
     */
    public function where(string $variable, string $pattern)
    {
        $route = end($this->routes);
        if ($route->variable_exist($variable))
            $route->setParamsPattern($variable, $pattern);

        return $this;
    }

    /**
     * Return response code 404
     */
    public static function returnNotFound()
    {
        http_response_code(404);
        exit('Ups... page not found!');
    }
}
