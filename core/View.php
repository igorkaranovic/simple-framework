<?php

namespace Core;

class View
{

    /**
     * @param string $view - name of file in Views folder (app/Views/) with extension
     * @param array $data
     */
    public static function render(string $view, array $data = [])
    {
        $file = APP_PATH . "/Views/{$view}";
        if (is_readable($file)) {
            extract($data, EXTR_SKIP);
            require_once($file);
        } else {
            echo "Unable to find view {$file}";
        }
    }
}