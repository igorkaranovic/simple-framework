<?php

namespace Core;

class Controller
{
    /**
     * All parameters passed through route
     * @var array
     */
    protected $route_params = [];

    /**
     * Is API route
     * @var bool
     */
    protected $isApi;

    public function __construct($route_params)
    {
        $this->route_params = $route_params;
    }

    /**
     * Call route action, with before&after filters
     *
     * @param $name
     * @param $arguments
     */
    public function __call($name, $arguments)
    {
        $method = $name . 'Action';
        if (!method_exists($this, $method))
            exit('Action not exist!');

        if ($this->before() !== false) {
            call_user_func_array([$this, "{$name}Action"], $this->route_params);
            $this->after();
        }
    }

    /**
     * Filter: will be called before any action
     */
    protected function before()
    {}

    /**
     * Filter: will be called after any action
     */
    protected function after()
    {}

    /**
     * Convert data to JSON and return
     *
     * @param array $data
     * @return string
     */
    protected function json($data = []) : string
    {
        return json_encode($data);
    }

    /**
     * Convert data to JSON string and print
     *
     * @param array $data
     */
    protected function returnJson($data = [])
    {
        echo $this->json($data);
        exit();
    }

    /**
     * Add data to result object, convert to JSON and print. Success is true in result object
     *
     * @param $data - data to return
     * @param string $message
     */
    protected function success($data = '', string $message = null) : void
    {
        $result = $this->getResult($data, $message ?? '', true);
        $this->returnJson($result);
    }

    /**
     * Add data to result object, convert to JSON and print. Success is false in result object
     *
     * @param $data - data to return
     * @param string $message - required
     */
    protected function abort($data, string  $message) : void
    {
        $result = $this->getResult($data, $message ?? '', false);
        $this->returnJson($result);
    }

    /**
     * Create and return result object
     *
     * @param $data
     * @param string $message
     * @param bool $success
     * @return object
     */
    private function getResult($data, string $message, bool $success) : object
    {
        $result = new \stdClass();
        $result->success = $success;
        $result->message = $message;
        $result->data = $data;

        return $result;
    }

}