<?php

namespace Core;

use Core\Interfaces\DatabaseInterface;
use PDO;

class Database implements DatabaseInterface
{

    protected $pdo;
    protected static $instance;
    protected $name;

    protected function __construct()
    {
        $this->connect();
        $this->name = CONFIG['name'];
    }

    /**
     * Return Database instance (if not exist create instance and return)
     * @return Database
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Connect to database
     */
    private function connect()
    {
        $dsn = "mysql:host=" . CONFIG['host'] . ";dbname=" . CONFIG['name'];
        try {
            $this->pdo = new PDO($dsn, CONFIG['user'], CONFIG['pass'], self::getPdoOptions());
        } catch (\PDOException $e) {
            exit('DB connection errod: ' . $e->getMessage());
        }
    }

    /**
     * Run provided SQL query, any arguments will be bind to query
     * If query is
     *
     *
     * @param string $sql - SQL query
     * @param array $args - arguments to bind
     * @return array|int|bool
     */
    public function run(string $sql, array $args = [])
    {
        $isSelect = strtoupper(substr($sql, 0, 6) === 'SELECT');
        $isInsert = strtoupper(substr($sql, 0, 6) === 'INSERT');

        $pdo = $this->executeSQL($sql, $args);
        if ($pdo === null)
            return false;

        if ($isSelect)
            return $pdo->fetchAll(PDO::FETCH_ASSOC);

        if ($isInsert)
            return $this->pdo->lastInsertId();

        return true;
    }

    /**
     * Execute SQL query, bind all values from $args
     *
     * @param string $sql
     * @param array $args
     * @return PDO|null
     */
    public function executeSQL(string $sql, array $args = [])
    {
        $pdo = $this->pdo->prepare($sql);

        foreach ($args as $key => $value) {
            $pdo->bindValue(":{$key}", $value);
        }

        if (!$pdo->execute())
            return null;

        return $pdo;
    }

    /**
     * Get items from db table
     *
     * @param string $table
     * @param string $condition
     * @param string $order_by
     * @param string $sort
     * @return array|bool|int
     */
    public function get($table, $condition = '', $order_by = '', $sort = '')
    {
        $where = $condition !== '' ? 'WHERE ' . $condition : '';
        $order_by = $order_by === '' ? 'id' : $order_by;
        $sort = $sort === '' ? 'DESC' : $sort;

        $sql = "SELECT * FROM `{$this->name}`.`{$table}` {$where} ORDER BY {$order_by} {$sort}";
        return $this->run($sql);
    }

    /**
     * Insert new item into database table
     *
     * @param string $table
     * @param array $data
     * @return array|bool|int
     */
    public function insert(string $table, array $data)
    {
        $fields = '';
        $values = '';

        foreach ($data as $field => $value) {
            $fields .= "`{$field}`, ";
            $values .= ":{$field}, ";
        }

        $fields = rtrim($fields, ', ');
        $values = rtrim($values, ', ');


        $sql = "INSERT INTO `{$this->name}`.`{$table}` ({$fields}) VALUES ({$values})";
        echo $sql;
        return $this->run($sql, $data);
    }


    /**
     * Update item by id
     *
     * @param string $table
     * @param $id
     * @param $data
     * @return bool|array
     */
    public function update($table, $id, $data)
    {
        $fields = '';
        foreach ($data as $field => $value)
            $fields .= "{$field} = :{$field}, ";

        $fields = rtrim($fields, ', ');

        $sql = "UPDATE `{$this->name}`.`{$table}` SET {$fields} WHERE id = {$id}";
        return $this->run($sql, $data);
    }

    /**
     * Delete item by id
     *
     * @param string $table
     * @param $id
     * @return bool|array
     */
    public function delete($table, $id)
    {
        $sql = "DELETE FROM `{$this->name}`.`{$table}` WHERE id = {$id}";
        return $this->run($sql);
    }

    /**
     * Get first matching item
     *
     * @param string $table
     * @param string $condition
     * @return bool|array
     */
    public function getFirst($table, $condition = '')
    {
        $where = $condition !== '' ? 'WHERE ' . $condition : '';
        $sql = "SELECT * FROM `{$this->name}`.`{$table}` {$where} LIMIT 1";
        $result = $this->run($sql);

        return !empty($result) ? $result[0] : false;
    }

    /**
     * Get database name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    private static function getPdoOptions() : array {
        return [
            PDO::ATTR_EMULATE_PREPARES => false,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING
        ];
    }
}