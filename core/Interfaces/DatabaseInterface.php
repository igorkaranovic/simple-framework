<?php
namespace Core\Interfaces;

interface DatabaseInterface
{

    public static function getInstance();

    public function run(string $sql, array $args);

    public function insert(string $table, array $data);

    public function get(string $table);

    public function update(string $table, int $id, array $data);

    public function delete(string $table, int $id);

    public function getFirst(string $table);
}
