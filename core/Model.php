<?php

namespace Core;

use Core\Interfaces\ModelInterface;
use Core;

class Model implements ModelInterface
{

    /**
     * Database singleton instance
     * @var Database
     */
    public $db;
    private $table;

//    public function __construct(IDatabase $database)
    public function __construct()
    {
        $this->db = Database::getInstance();
    }

    /**
     * Set table for current model
     *
     * @param string $table
     * @return $this
     */
    public function setTable(string $table)
    {
        $this->table = $table;
        return $this;
    }

    /**
     * Insert new row in database table
     *
     * @param array $data
     * @return bool
     */
    public function insert(array $data)
    {
        return $this->db->insert($this->table, $data);
    }


    /**
     * Get rows from table
     *
     * @param string $condition
     * @param string $order_by
     * @param string $sort
     * @return bool
     */
    public function get($condition = '', $order_by = 'id', $sort = 'DESC')
    {
        return $this->db->get($this->table, $condition, $order_by, $sort);
    }

    /**
     * Get first matching row from database
     *
     * @param string $condition
     * @return array|bool
     */
    public function getFirst($condition = '')
    {
        return $this->db->getFirst($this->table, $condition);
    }

    /**
     * Get single column
     *
     * @param string $sql
     * @param array $args
     * @return string|bool
     */
    public function getColumn(string $sql, array $args = [])
    {
        $result = $this->db->executeSQL($sql, $args);
        if ($result === false)
            return false;

        return $result->fetchColumn();
    }

    /**
     * Get all rows from table
     *
     * @param string $order_by
     * @param string $sort
     * @return bool
     */
    public function getAll($order_by = 'id', $sort = 'DESC')
    {
        return $this->db->get($this->table, '', $order_by, $sort);
    }

    /**
     * Delete item from table
     *
     * @param int $id - id of item to delete
     * @return bool
     */
    public function delete(int $id)
    {
        return $this->db->delete($this->table, $id);
    }

    /**
     * Update single item by id
     *
     * @param int $id - id of item to update
     * @param array $data - keys must exists be same as table columns
     * @return bool
     */
    public function update(int $id, array $data)
    {
        return $this->db->update($this->table, $id, $data);
    }

    /**
     * @return string
     */
    public function getDatabaseName() : string
    {
        return $this->db->getName();
    }

}