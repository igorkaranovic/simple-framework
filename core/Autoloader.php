<?php
namespace Core;

class Autoloader
{

    /**
     * Autoload classes
     */
    public static function register()
    {
        spl_autoload_register(function ($class) {
            $class = str_replace('\\', '/', $class);
            $file = ROOT_PATH . "/{$class}.php";

            if (is_readable($file))
                require_once($file);
        });
    }

}