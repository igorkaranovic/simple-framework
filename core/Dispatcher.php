<?php

namespace Core;

class Dispatcher
{

    private $controller;
    private $method;
    private $params;
    private $isApi;

    /**
     * Set controller to dispatch
     *
     * @param string $controller
     * @return Dispatcher
     */
    public function setController(string $controller) : Dispatcher
    {
        $this->controller = $controller;
        return $this;
    }

    /**
     * Set method to dispatch
     *
     * @param string $method
     * @return Dispatcher
     */
    public function setMethod(string $method) : Dispatcher
    {
        $this->method = $method;
        return $this;
    }

    /**
     * Set route params
     *
     * @param array $params
     * @return Dispatcher
     */
    public function setParams(array $params) : Dispatcher
    {
        $this->params = $params;
        return $this;
    }


    /**
     * Set is route API
     *
     * @param bool $value
     * @return Dispatcher
     */
    public function isApi(bool $value) : Dispatcher
    {
        $this->isApi = $value;
        return $this;
    }

    /**
     * Try to get controller, set route parameters and call method
     * @throws \Exception
     */
    public function dispatch()
    {
        $controllerName = "App\Controllers\\{$this->controller}";

        if (!class_exists($controllerName))
            throw new \Exception('Unknown controller!');

        $controller = new $controllerName($this->params);
        $method = $this->method;

        if (!is_callable([$controller, $method]))
            throw new \Exception('Method is unavailable!');

        header($this->getContentType());

        if (!empty($this->params)) {
            call_user_func_array([$controller, $method], $this->params);
        } else {
            $controller->$method();
        }
    }

    private function getContentType() : string
    {
        return $this->isApi ? 'Content-type:application/json' : 'Content-type:text/html';
    }
}
