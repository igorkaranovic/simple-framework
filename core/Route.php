<?php

namespace Core;

class Route {
    public $route;
    public $controller;
    public $method;
    public $isApi;

    public $segments = [];
    public $segmentsNumber = 0;
    public $params = [];

    public function __construct(string $url = '') {
        if ($url !== '')
            $this->setRoute($url);
    }

    /**
     * @param string $route
     * @return Route
     */
    public function setRoute(string $route) : Route {
        $this->route = $this->formatUrl($route);
        $this->get_route_segments();
        $this->get_route_vars();

        return $this;
    }

    /**
     * @param string $action
     * @return Route
     */
    public function setController(string $action) : Route {
        $this->parse_controller($action);

        return $this;
    }

    /**
     * @param bool $value
     * @return $this
     */
    public function setIsApi(bool $value) {
        $this->isApi = $value;
        return $this;
    }

    /**
     * @param string $variable
     * @param string $pattern
     * @return Route
     * @throws Exception
     */
    public function setParamsPattern(string $variable, string $pattern) : Route
    {
        if (!$this->variable_exist($variable))
            throw new Exception('Unknown variable in route');

        $this->params[$variable]['pattern'] = $pattern;
        return $this;
    }

    /**
     * Get all params as name => value array
     *
     * @return array
     */
    public function getParams() : array {
        $params = [];

        foreach ($this->params as $name => $data)
            $params[$name] = $data['value'];

        return $params;
    }

    /**
     * @return mixed
     */
    public function getIsApi() {
        return $this->isApi === true;
    }

    public function dispatch() {
        $dispatcher = new Dispatcher();
        $dispatcher->setController($this->controller)
            ->setMethod($this->method)
            ->setParams($this->getParams())
            ->isApi($this->getIsApi())
            ->dispatch();
    }

    public function variable_exist(string $name) : bool {
        return isset($this->params[$name]);
    }

    private function get_route_segments() : void {
        $this->segments = explode('/', $this->route);
        $this->segmentsNumber = count($this->segments);
    }

    private function get_route_vars() : void
    {
        foreach ($this->segments as $segment) {
            if (!preg_match('/^:[\w]+$/', $segment))
                continue;

            $name = ltrim($segment, ':');
            $this->params[$name] = [];
        }
    }

    /**
     * @param string $controller
     * @return array
     * @throws Exception
     */
    private function parse_controller(string $controller) {
        $parts = explode('@', $controller);
        if (count($parts) !== 2)
            throw new Exception('Router error: wrong router controller format!');

        $this->controller = $parts[0];
        $this->method = $parts[1];
    }

    /**
     * Check does requested route match route
     * route must match all parts, if route have defined params, it will be checked
     *
     * @param Route $current - requested route
     * @param Route $route - registered route
     * @return bool
     */
    public static function compare(Route $current, Route $route) : bool {
        $params = [];

        for($i = 0; $i < $route->segmentsNumber; $i ++) {
            $segment = $route->segments[$i];
            $isParam = substr($segment, '0', '1') === ':';

            // segment is not parameter
            if (!$isParam) {
                if ($current->segments[$i] !== $segment)
                    return false;

                // current segment is same, continue
                continue;
            }

            // check does param have regex pattern, if not, pass
            $name = ltrim($segment, ':');
            if (!isset($route->params[$name]['pattern'])) {
                continue;
            }

            if (!self::paramPatternMatch($route, $name, $current->segments[$i]))
                return false;

            $params[$name] = $current->segments[$i];
        }

        self::addParamValuesToRoute($route, $params);
        return true;
    }

    /**
     * Add params values from request to current route
     *
     * @param Route $route
     * @param array $params
     */
    private static function addParamValuesToRoute(Route $route, array $params) : void {
        foreach ($params as $name => $value) {
            if (isset($route->params[$name]))
                $route->params[$name]['value'] = $value;
        }
    }

    /**
     * Check does param value > pattern match
     *
     * @param Route $route
     * @param string $param
     * @param string $value
     * @return bool
     */
    private static function paramPatternMatch(Route $route, string $param, string $value) : bool {
        $pattern = $route->params[$param]['pattern'];

        return preg_match("/^{$pattern}$/", $value) === 1;
    }

    /**
     * @param string $url
     * @return string
     */
    private function formatUrl(string $url): string
    {
        $url = trim($url, '/');
        $url = $this->removeQueryString($url);

        return $url;
    }

    /**
     * @param string $url
     * @return string
     */
    private function removeQueryString(string $url): string
    {
        if (strpos($url, '?')) {
            $parts = explode('?', $url);
            return $parts[0];
        }

        return $url;
    }

}
